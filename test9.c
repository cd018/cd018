
#include <stdio.h>

struct name
{
    char  Fname[25];
    char  Lname[25];
};
struct student
{
    int roll_no;
    struct name n;
    char department[3];
    float fees;
    char section[2];
    int TotalMarks;
};
int main()
{
    int i;
    struct  student a[2];
    for(i=0;i<2;i++)
    {
        printf("Details of student:%d -->\n",i+1);
        printf("Enter the name:\n");
        scanf("%s",&a[i].n.Fname);
        scanf("%s",&a[i].n.Lname);
        printf("Enter the roll no:\n");
        scanf("%d",&a[i].roll_no);
        printf("Enter the section:\n");
        scanf("%s",&a[i].section[i]);
        printf("Enter the department:\n");
        scanf("%s",&a[i].department);
        printf("Enter the fees\n");
        scanf("%f",&a[i].fees);
        printf("Total marks out of 500:\n");
        scanf("%d",&a[i].TotalMarks);
        printf("\n");
    }

    printf("Student Details :\n");
    printf("\n");
    for(i=0;i<2;i++)
    {
        printf("Name: %s %s\n",a[i].n.Fname,a[i].n.Lname);
        printf("Roll no: %d\n",a[i].roll_no);
        printf("Section:%c\n",a[i].section[i]);
        printf("Department: %s\n",a[i].department);
        printf("Fees:%f \n",a[i].fees);
        printf("Total marks: %d\n",a[i].TotalMarks);
        printf("\n");
    }
    if(a[0].TotalMarks>a[1].TotalMarks)
    {
        printf("Student1 has scored more: %d",a[0].TotalMarks);
    }
    else
    {
        printf("Student2 has scored more: %d",a[1].TotalMarks);
    }
    return 0;
}